use std::{
    future::Future,
    path::{Path, PathBuf},
    pin::Pin,
    sync::Arc,
};

use chrono::NaiveDateTime;
use ilias_soap::{CourseInfo, Ilias, Object, ObjectType, RefId};
use tokio::{sync::mpsc, task::JoinHandle};

use crate::status::{tree_build_progress, TreeNodeKind, TreeStatusMessage};

pub async fn gather_downloads(
    ilias: &Arc<Ilias>,
    base_path: &Path,
    courses: Vec<CourseInfo>,
    newer_than: Option<NaiveDateTime>,
    target_semester: Option<String>,
) -> Vec<Download> {
    let base_path = base_path.to_owned();
    let (status_channel, printer_task) = {
        let (send, recv) = mpsc::channel::<TreeStatusMessage>(10);
        let base = base_path.clone();

        let printer_task = tokio::spawn(async move { tree_build_progress(base, recv).await });

        (send, printer_task)
    };

    let handles = courses
        .into_iter()
        .map(|course| -> JoinHandle<anyhow::Result<Vec<Download>>> {
            let ilias = ilias.clone();
            let base_path = base_path.clone();
            let status_channel = status_channel.clone();
            let target_semester = target_semester.clone();
            tokio::spawn(async move {
                let title = &course.course.meta_data.title;

                let parts = title
                    .strip_prefix('[')
                    .and_then(|title| title.find(']').map(|i| (&title[0..i], &title[i + 1..])))
                    .map(|(semester, name)| {
                        (
                            semester.replace('/', "_"),
                            name.trim_start().replace('/', "_"),
                        )
                    });

                let path = if let Some((semester, name)) = &parts {
                    base_path.join(semester).join(name)
                } else {
                    base_path.join(title.replace('/', "_"))
                };

                let semester = parts.as_ref().map(|(semester, _)| semester);

                let correct_semester = semester
                    .zip(target_semester)
                    .map_or(true, |(semester, target_semester)| {
                        semester == &target_semester
                    });

                if !correct_semester {
                    return Ok(Vec::new());
                }

                let mut downloads = Vec::<Download>::new();
                build_folder_downloads(
                    &ilias,
                    course.ref_id,
                    &path,
                    &mut downloads,
                    newer_than,
                    &status_channel,
                )
                .await?;

                Ok(downloads)
            })
        });

    let download = futures::future::join_all(handles)
        .await
        .into_iter()
        .flat_map(|x| x.unwrap().unwrap().into_iter())
        .filter(|download| download.object.can_access)
        .collect();

    drop(status_channel);

    printer_task.await.unwrap();

    download
}

fn build_folder_downloads<'a>(
    ilias: &'a Ilias,
    folder: RefId,
    path: &'a Path,
    downloads: &'a mut Vec<Download>,
    newer_than: Option<NaiveDateTime>,
    status: &'a mpsc::Sender<TreeStatusMessage>,
) -> Pin<Box<dyn Future<Output = anyhow::Result<()>> + 'a + Send>> {
    Box::pin(async move {
        let _ = tokio::fs::create_dir_all(path).await;

        let objects = ilias
            .get_tree_children(
                folder,
                &[
                    ObjectType::Folder,
                    ObjectType::OpenCast,
                    ObjectType::File,
                    ObjectType::MediaCast,
                    ObjectType::Group,
                    ObjectType::Excercise,
                    // ObjectType::Session,
                    ObjectType::WebReference,
                ],
            )
            .await?;

        // dbg!(&objects);

        // TODO: push object_path for files instead of path?
        for object in objects {
            let object_path = path.join(object.title.trim().replace('/', "_"));

            if let Some(newer_than) = newer_than {
                if object.last_update < newer_than {
                    match object.r#type {
                        ObjectType::Folder | ObjectType::Group | ObjectType::Session => {
                            status
                            .send(TreeStatusMessage {
                                path: object_path.clone(),
                                active: false,
                                kind: TreeNodeKind::Folder,
                            })
                            .await
                            .ok()
                            .unwrap();
                        }
                        
                        _ => {
                            status
                            .send(TreeStatusMessage {
                                path: object_path.clone(),
                                active: false,
                                kind: match object.r#type {
                                    ObjectType::File => TreeNodeKind::File,
                                    ObjectType::MediaCast | ObjectType::OpenCast => TreeNodeKind::Videos,
                                    ObjectType::Group => TreeNodeKind::Group,
                                    
                                    kind => unreachable!("the SOAP API returned an object of type {kind} we did not request"),
                                },
                            })
                            .await
                            .ok()
                            .unwrap();
                        }
                    }

                    continue;
                }
            }

            match object.r#type {
                ObjectType::Folder | ObjectType::Group | ObjectType::Session => {
                    status
                        .send(TreeStatusMessage {
                            path: object_path.clone(),
                            active: true,
                            kind: match object.r#type {
                                ObjectType::Folder => TreeNodeKind::Folder,
                                ObjectType::Group => TreeNodeKind::Group,
                                ObjectType::Session => TreeNodeKind::Session,
                                _ => unreachable!(),
                            },
                        })
                        .await
                        .ok()
                        .unwrap();

                    build_folder_downloads(
                        ilias,
                        object.ref_id,
                        &object_path,
                        downloads,
                        newer_than,
                        status,
                    )
                    .await?;
                }
                ObjectType::File | ObjectType::OpenCast | ObjectType::MediaCast | ObjectType::Excercise | ObjectType::WebReference=> {
                    status
                        .send(TreeStatusMessage {
                            path: object_path.clone(),
                            active: true,
                            kind: match object.r#type {
                                ObjectType::File => TreeNodeKind::File,
                                ObjectType::Excercise => TreeNodeKind::Excercise,
                                ObjectType::OpenCast | ObjectType::MediaCast => TreeNodeKind::Videos,
                                ObjectType::WebReference => TreeNodeKind::WebReference,

                                _ => unreachable!()
                            },
                        })
                        .await
                        .ok()
                        .unwrap();

                    downloads.push(Download {
                        object,
                        folder: path.to_owned(),
                    });
                }
                

                _ => unreachable!("the SOAP API returned an object type we did not request"),
            }
        }

        Ok(())
    })
}

#[derive(Debug)]
pub struct Download {
    pub folder: PathBuf,
    pub object: Object,
}
