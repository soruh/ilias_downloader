use std::{
    collections::VecDeque,
    sync::{
        atomic::{AtomicUsize, Ordering},
        Arc, Mutex,
    },
};

use once_cell::sync::Lazy;
use tokio::sync::oneshot;

type Waiters = Arc<Mutex<VecDeque<oneshot::Sender<()>>>>;

const N_INITIAL_PERMITS: usize = 8;

static N_PERMITS: AtomicUsize = AtomicUsize::new(N_INITIAL_PERMITS);
static PERMITS: AtomicUsize = AtomicUsize::new(N_INITIAL_PERMITS);
static WAITERS: Lazy<Waiters> = Lazy::new(|| Arc::new(Mutex::new(VecDeque::new())));

pub struct Permit(Waiters);
impl Drop for Permit {
    fn drop(&mut self) {
        let _n = PERMITS.fetch_add(1, Ordering::SeqCst);
        // eprintln!("available download permits: {}", _n + 1);
        if let Some(waiter) = self.0.lock().unwrap().pop_front() {
            waiter.send(()).unwrap();
        }
    }
}

#[allow(clippy::redundant_else)] // false positive?
pub async fn aquire_download_permit() -> Permit {
    loop {
        let n_permits = PERMITS.load(Ordering::SeqCst);
        if n_permits > 0 {
            if PERMITS
                .compare_exchange_weak(
                    n_permits,
                    n_permits - 1,
                    Ordering::SeqCst,
                    Ordering::Relaxed,
                )
                .is_ok()
            {
                // eprintln!("available download permits: {}", n_permits - 1);
                return Permit(WAITERS.clone());
            } else {
                continue;
            }
        }

        let (send, recv) = oneshot::channel();
        WAITERS.lock().unwrap().push_back(send);
        recv.await.unwrap();
    }
}

#[allow(clippy::cast_sign_loss, clippy::cast_possible_wrap)]
pub fn rescale_permits(n_permits: usize) {
    // eprintln!("rescaling to {} permits", n_permits);
    loop {
        let n = N_PERMITS.load(Ordering::SeqCst);

        if N_PERMITS
            .compare_exchange_weak(n, n_permits, Ordering::SeqCst, Ordering::Relaxed)
            .is_err()
        {
            continue;
        }

        let n_additional = n_permits as isize - n as isize;
        if n_additional >= 0 {
            PERMITS.fetch_add(n_additional as usize, Ordering::SeqCst);
        } else {
            PERMITS.fetch_sub((-n_additional) as usize, Ordering::SeqCst);
        }

        break;
    }

    // eprintln!("rescaled to {} permits", n_permits);
}
