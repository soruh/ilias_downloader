use std::sync::{Arc, Mutex};

use futures::{future::join_all, StreamExt};
use ilias_soap::{Ilias, RefId};
use indicatif::ProgressBar;
use tokio::io::AsyncWriteExt;

use crate::{
    build_download_list::Download,
    download_throttle::aquire_download_permit,
    metadata::{is_file_stale, MetaData, MetaDataCache, MetaDataId},
    status::{bar_style, BarKind},
};

pub async fn download(
    ilias: &Arc<Ilias>,
    metadata: &Arc<Mutex<MetaDataCache>>,
    download_client: &Arc<reqwest::Client>,
    files: Vec<Download>,
) -> anyhow::Result<Vec<(u64, Download)>> {
    let files: Vec<_> = files
        .into_iter()
        .filter(|file| is_file_stale(file, &metadata.lock().unwrap()))
        .collect();

    if files.is_empty() {
        return Ok(Vec::new());
    }

    let pb = ProgressBar::new(files.len() as u64).with_style(bar_style(BarKind::Numerical));
    pb.set_message("downloading files");

    let results = join_all(files.into_iter().map(|download| {
        let pb = pb.clone();
        async move {
            let _permit = aquire_download_permit().await;

            let url = build_file_link(ilias, download.object.ref_id);

            // let get_file_start = std::time::Instant::now();

            let response = download_client.get(url).send().await.unwrap();

            response.error_for_status_ref()?;

            let url = response.url();
            if url.path() == "/ilias/login.php"
                || url
                    .query_pairs()
                    .any(|(title, value)| title == "ref_id" && value == "1")
            {
                // We are not allowed to access this file
                pb.inc(1);
                return Ok(None);
            }

            let filename = if let Some(content_disposition) =
                response.headers().get("content-disposition")
            {
                let content_disposition =
                    if let Ok(s) = std::str::from_utf8(content_disposition.as_bytes()) {
                        s
                    } else {
                        println!("failed to decode content_disposition: {content_disposition:?}\n");

                        return Ok(None);
                    };

                content_disposition
                    .split(';')
                    .find_map(|part| part.split('=').nth(1))
                    .unwrap()
                    .strip_prefix('"')
                    .unwrap()
                    .strip_suffix('"')
                    .unwrap()
                    .to_owned()
            } else {
                panic!(r#"missing "content-disposition" header."#)

                /*
                let mime: mime::Mime = response
                    .headers()
                    .get("content-type")
                    .unwrap()
                    .to_str()
                    .unwrap()
                    .parse()
                    .unwrap();

                let ext = match mime.essence_str() {
                    "text/html" => ".html",
                    x => panic!("unecpected mime type: {}", x),
                };

                let mut filename = download.object.title.clone();
                if !filename.ends_with(ext) {
                    filename.push_str(ext);
                }

                filename
                */
            };

            let path = download.folder.join(&filename);

            assert!(
                !filename.contains('/'),
                "filename contains '/' (TODO: handle this case or determine it is unreachable)"
            );

            let mut file = tokio::fs::File::create(&path).await.unwrap();

            let mut stream = response.bytes_stream();

            let mut file_size = 0;
            while let Some(bytes) = stream.next().await {
                let bytes = bytes?;

                file_size += bytes.len() as u64;

                file.write_all(&*bytes).await.unwrap();
            }

            drop(file);

            // eprintln!("downloading {:75} took {:#?}", filename,
            // get_file_start.elapsed());

            let mtime = download.object.last_update.timestamp();

            filetime::set_file_mtime(&path, filetime::FileTime::from_unix_time(mtime, 0))?;

            metadata.lock().unwrap().insert(
                &MetaDataId::File(download.object.ref_id),
                MetaData { mtime, path },
            );

            pb.inc(1);

            Ok(Some((file_size, download)))
        }
    }))
    .await
    .into_iter()
    .filter_map(Result::transpose)
    .collect::<anyhow::Result<Vec<(u64, Download)>>>()?;

    pb.finish();

    Ok(results)
}

fn build_file_link(ilias: &Ilias, ref_id: RefId) -> String {
    format!(
        "{}goto_{}_file_{}_download.html",
        ilias.base_url,
        ilias.client(),
        ref_id.0
    )
}
