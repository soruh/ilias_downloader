use std::{io::Write, path::PathBuf};

use debug_tree::TreeBuilder;
use tokio::sync::mpsc::Receiver;

macro_rules! escape_sequence {
    (INTERNAL) => {
        ""    
    };

    (INTERNAL $single: literal) => {
        stringify!($single)
    };

    (INTERNAL $head: literal, $($tail: literal),+) => {
        concat!(stringify!($head), ";", escape_sequence!(INTERNAL $($tail),*))
    };

    ($($n: literal,)* $kind: ident $(,)?) => {
        concat!(
            "\x1b[",
            escape_sequence!(INTERNAL $($n),*),
            stringify!($kind),
        )
    };
}

const CLEAR_COLOR: &str = escape_sequence!(m);

pub fn unicode_truncate(s: &str, mut len: usize) -> &str {
    while !s.is_char_boundary(len) {
        len -= 1;
    }

    &s[..len]
}

#[derive(Clone, Copy)]
pub enum BarKind {
    Bytes,
    Numerical,
    Time,
}

pub const BAR_WIDTH: usize = 20;

pub fn bar_style(kind: BarKind) -> indicatif::ProgressStyle {
    indicatif::ProgressStyle::default_bar()
        .progress_chars("=>–")
        .template(
            &match kind {
                BarKind::Bytes     => format!("[{{elapsed_precise}}|{{eta_precise}}] {{bar:{}.bright.green/bright.yellow}} {{bytes_per_sec:>12.blue}}|{{bytes:>10.green}}/{{total_bytes:>10.yellow}} {{wide_msg}}", BAR_WIDTH),
                BarKind::Numerical => format!("[{{elapsed_precise}}|{{eta_precise}}] {{bar:{}.bright.green/bright.yellow}} {{percent:>4.blue}}% {{pos:>5.green}}/{{len:>5.yellow}} {{wide_msg}}", BAR_WIDTH),
                BarKind::Time      => format!("[{{elapsed_precise}}|{{eta_precise}}] {{bar:{}.bright.green/bright.yellow}} {{per_sec:>5.blue}} {{pos:>5.green}}/{{len:>5.yellow}} {{wide_msg}}", BAR_WIDTH),
            }
        )
}

#[derive(Debug)]
pub struct TreeStatusMessage {
    pub path: PathBuf,
    pub active: bool,
    pub kind: TreeNodeKind,
}

#[derive(Debug, Clone, Copy)]
pub enum TreeNodeKind {
    Folder,
    File,
    Videos,
    Group,
    Excercise,
    Session,
    WebReference,
}

#[derive(Debug)]
struct Node {
    name: String,
    children: Vec<Node>,
    active: bool,
    depth: usize,
    kind: TreeNodeKind,
    collapsed: bool,
}

impl Node {
    fn expand_all(&mut self) {
        self.collapsed = false;

        for child in &mut self.children {
            child.expand_all();
        }
    }

    fn children_recursive(&self) -> usize {
        1 + self
            .children
            .iter()
            .map(Node::children_recursive)
            .sum::<usize>()
    }

    fn height_recursive(&self) -> usize {
        1 + (!self.collapsed)
            .then(|| {
                self.children
                    .iter()
                    .map(Node::height_recursive)
                    .sum::<usize>()
            })
            .unwrap_or(1)
    }

    fn add_to_tree(&self, tree: &mut TreeBuilder, size: (usize, usize)) {
        let (width, _height) = size;

        let padding_width = self.depth * 2 + 1;

        let name = format!(
            "{}{}{}",
            self.color(),
            unicode_truncate(&self.name, width - padding_width),
            CLEAR_COLOR
        );

        if self.children.is_empty() {
            tree.add_leaf(&name);
        } else {
            let mut branch = tree.add_branch(&name);

            if self.collapsed {
                tree.add_leaf(&format!("[{} children]", self.children_recursive(),));
            } else {
                for child in &self.children {
                    child.add_to_tree(tree, size);
                }
            }

            branch.release();
        }
    }

    fn get_biggest_node(&mut self, only_inactive: bool) -> Option<&mut Node> {
        self.children
            .iter_mut()
            .filter(|node| !node.collapsed)
            .filter(|node| if only_inactive { !node.active } else { true })
            .map(|node| (node.height_recursive(), node))
            .max_by_key(|&(height, _)| height)
            .map(|(_, biggest_node)| biggest_node)
    }

    fn collapse_biggest(&mut self) {
        let biggest_node = if let Some(node) = self.get_biggest_node(true) {
            Some(node)
        } else {
            self.get_biggest_node(false)
        };

        if let Some(biggest_node) = biggest_node {
            biggest_node.collapse_biggest();
        } else {
            self.collapsed = true;
        }
    }

    fn print(&mut self, tree: &mut TreeBuilder, size: (usize, usize)) {
        let (_width, height) = size;

        self.expand_all();

        while self.height_recursive() > height - 1 {
            self.collapse_biggest();
        }

        self.add_to_tree(tree, size);
    }
}

pub async fn tree_build_progress(base: PathBuf, mut recv: Receiver<TreeStatusMessage>) {
    let mut root = Node {
        name: String::new(),
        children: Vec::new(),
        active: false,
        depth: 0,
        kind: TreeNodeKind::Folder,
        collapsed: false,
    };

    while let Some(TreeStatusMessage { path, active, kind }) = recv.recv().await {
        let path = path.strip_prefix(&base).unwrap();

        let mut tree = debug_tree::tree("Courses");

        let mut node = &mut root;

        let n_components = path.components().count();

        for (component_i, component) in path.components().enumerate() {
            let component = component.as_os_str().to_str().unwrap();

            let i = node
                .children
                .iter_mut()
                .enumerate()
                .find_map(|(i, x)| (x.name == component).then(|| i));

            if let Some(i) = i {
                if active {
                    node.children[i].active = true;
                }

                node = &mut node.children[i];
            } else {
                let kind = if component_i == n_components - 1 {
                    kind
                } else {
                    TreeNodeKind::Folder
                };
                let new_node = Node {
                    name: component.to_owned(),
                    children: Vec::new(),
                    active,
                    kind,
                    depth: node.depth + 1,
                    collapsed: false,
                };

                node.children.push(new_node);
                node = node.children.last_mut().unwrap();
            }
        }

        let size = console::Term::stderr().size();
        let size = (size.1 as usize, size.0 as usize);

        root.print(&mut tree, size);

        print!(concat!(escape_sequence!(0, 0, H), escape_sequence!(2, J)));
        std::io::stdout().flush().unwrap();
        tree.print();
    }
}

impl Node {
    fn color(&self) -> &'static str {
        if self.active {
            use TreeNodeKind::*;
            match self.kind {
                Folder => escape_sequence!(34, 1, m),
                File => escape_sequence!(m),
                Videos => escape_sequence!(36, 1, m),
                Group => escape_sequence!(33, 1, m),
                Excercise => escape_sequence!(35, 1, m),
                Session => escape_sequence!(m),
                WebReference => escape_sequence!(34, m),
            }
        } else {
            escape_sequence!(33, m)
        }
    }
}
