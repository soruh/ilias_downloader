use std::{
    sync::{Arc, Mutex},
    time::Duration,
};

use chrono::NaiveTime;
use futures::{future::join_all, StreamExt};
use ilias_soap::{Ilias, RefId};
use indicatif::{MultiProgress, ProgressBar, ProgressDrawTarget};
use tokio::io::AsyncWriteExt;

use crate::{
    build_download_list::Download,
    download_throttle::{aquire_download_permit, rescale_permits},
    metadata::{is_mediacast_video_stale, MetaData, MetaDataCache, MetaDataId},
    status::{bar_style, BarKind},
    VideoDownload,
};

#[derive(Debug)]
pub struct MediaCastDownload {
    pub download: Arc<Download>,
}

fn build_url(ilias: &Arc<Ilias>, ref_id: RefId) -> String {
    format!(
        "{}ilias.php?baseClass=ilMediaCastHandlerGUI&ref_id={}&cmd=showContent",
        ilias.base_url, ref_id.0
    )
}

pub async fn download(
    ilias: &Arc<Ilias>,
    metadata: &Arc<Mutex<MetaDataCache>>,
    download_client: &Arc<reqwest::Client>,
    videos: Vec<Download>,
) -> anyhow::Result<Vec<VideoDownload>> {
    mod selector {
        use once_cell::sync::Lazy as L;
        use scraper::Selector as S;

        pub static TABLE: L<S> = L::new(|| S::parse("div.ilTableOuter table").unwrap());
        pub static TBODY: L<S> = L::new(|| S::parse("tbody").unwrap());
        pub static TR: L<S> = L::new(|| S::parse("tr").unwrap());
        pub static TD: L<S> = L::new(|| S::parse("td").unwrap());
        pub static A: L<S> = L::new(|| S::parse("a").unwrap());
        pub static P: L<S> = L::new(|| S::parse("p").unwrap());
        pub static B: L<S> = L::new(|| S::parse("b").unwrap());
    }

    let urls = join_all(videos.into_iter().map(|video| async move {
        let url = build_url(ilias, video.object.ref_id);

        let video = Arc::new(video);

        let document = download_client.get(url).send().await?.text().await?;

        let html = scraper::Html::parse_document(&document);

        let table = {
            let mut tables = html.select(&selector::TABLE);
            let table = tables.next().unwrap();
            assert_eq!(tables.next(), None);
            table
        };

        let body = table.select(&selector::TBODY).next().unwrap();

        let rows = body.select(&selector::TR);

        let downloads = rows
            .map(|tr| {
                let mut collumns = tr.select(&selector::TD);

                let title = collumns
                    .next()
                    .unwrap()
                    .text()
                    .map(str::trim)
                    .collect::<String>();

                let properties = collumns.next().unwrap();

                let duration = properties
                    .select(&selector::P)
                    .find_map(|p| {
                        let mut bs = p.select(&selector::B);

                        let b = bs.next().unwrap();
                        assert_eq!(bs.next(), None);

                        let label = b
                            .text()
                            .collect::<String>()
                            .split_whitespace()
                            .collect::<Vec<_>>()
                            .join(" ");

                        (label == "Play Time").then(|| {
                            let text = p.text().map(str::trim).collect::<String>();

                            let (i, _) = text.char_indices().find(|&(_, c)| c == ':').unwrap();
                            let time_text = text[i + 1..].trim();

                            let time = NaiveTime::parse_from_str(time_text, "%H:%M:%S").ok();

                            time.map(|time| {
                                time.signed_duration_since(NaiveTime::from_hms(0, 0, 0))
                                    .to_std()
                                    .unwrap()
                            })
                        })
                    })
                    .unwrap();

                let anchor = properties
                    .select(&selector::A)
                    .find(|anchor| anchor.text().map(str::trim).collect::<String>() == "Download")
                    .unwrap();

                let href = anchor.value().attr("href").unwrap().to_owned();

                (
                    video.clone(),
                    title,
                    format!("{}{}", ilias.base_url, href),
                    duration,
                )
            })
            .collect::<Vec<_>>();

        Ok(downloads)
    }))
    .await
    .into_iter()
    .collect::<anyhow::Result<Vec<Vec<(Arc<Download>, String, String, Option<Duration>)>>>>()?
    .into_iter()
    .flatten()
    .filter(|(download, title, _, _)| {
        is_mediacast_video_stale(download, &metadata.lock().unwrap(), title.clone())
    })
    .collect::<Vec<_>>();

    if urls.is_empty() {
        return Ok(Vec::new());
    }

    let multi_bar = Arc::new(MultiProgress::with_draw_target(
        ProgressDrawTarget::stderr_nohz(),
    ));

    let mb = multi_bar.clone();
    let new_bar = Arc::new(move |kind: BarKind, len: u64| -> ProgressBar {
        mb.add(indicatif::ProgressBar::new(len).with_style(bar_style(kind)))
    });

    let mb = multi_bar.clone();
    let new_spinner =
        Arc::new(move || -> ProgressBar { mb.add(indicatif::ProgressBar::new_spinner()) });

    // HACK: force rendering of empty multi bar
    let _dummy_spinner = new_spinner();

    let mb = multi_bar.clone();
    tokio::task::spawn_blocking(move || mb.join().unwrap());

    let total_bar = new_bar(BarKind::Bytes, 0);
    total_bar.set_message("Total");

    rescale_permits(8);

    join_all(urls.into_iter().map(|(download, title, url, duration)| {
        let new_bar = new_bar.clone();
        let metadata = metadata.clone();
        let total_bar = total_bar.clone();
        async move {
            let folder = &download.folder;

            tokio::fs::create_dir_all(folder).await?;

            let _permit = aquire_download_permit().await;

            let response = download_client.get(url).send().await.unwrap();

            let headers = response.headers();

            let content_length: u64 = headers
                .get("content-length")
                .unwrap()
                .to_str()
                .unwrap()
                .parse()
                .unwrap();

            /*
            let last_modified = headers.get("last-modified").unwrap().to_str().unwrap();
            let last_modified = NaiveDateTime::parse_from_str(last_modified, "%a, %d %b %Y %H:%M:%S GMT").unwrap();
            */

            let own_bar = new_bar(BarKind::Bytes, content_length);
            own_bar.set_message(title.clone());
            total_bar.inc_length(content_length);

            let filename = {
                headers
                    .get("content-disposition")
                    .and_then(|content_disposition| {
                        content_disposition
                            .to_str()
                            .ok()
                            .and_then(|content_disposition| {
                                content_disposition.split(';').find_map(|x| {
                                    let (i, _) = x.char_indices().find(|&(_, c)| c == '=')?;

                                    Some(
                                        x[i + 1..]
                                            .trim_start_matches('"')
                                            .trim_end_matches('"')
                                            .to_owned(),
                                    )
                                })
                            })
                    })
                    .unwrap_or_else(|| format!("{}.mp4", title.trim().replace('/', "_")))
            };

            let path = folder.join(filename);

            let mut file = tokio::fs::File::create(&path).await?;
            let mut stream = response.bytes_stream();

            while let Some(chunk) = stream.next().await {
                let chunk = chunk?;

                own_bar.inc(chunk.len() as u64);
                total_bar.inc(chunk.len() as u64);

                file.write_all(&chunk).await?;
            }

            let mtime = download.object.last_update.timestamp();

            filetime::set_file_mtime(&path, filetime::FileTime::from_unix_time(mtime, 0))
                .expect("Failed to set file mtime");

            // last_modified.timestamp() as u64,
            metadata.lock().unwrap().insert(
                &MetaDataId::Video(download.object.ref_id, title.clone()),
                MetaData { mtime, path },
            );

            Ok(Some(VideoDownload { title, duration }))
        }
    }))
    .await
    .into_iter()
    .filter_map(Result::transpose)
    .collect::<anyhow::Result<Vec<VideoDownload>>>()
}
