use std::{
    path::Path,
    sync::{Arc, Mutex},
    time::Duration,
};

use chrono::{DateTime, Local};
use futures::future::{join_all, Either};
use ilias_soap::{Ilias, RefId};
use indicatif::{MultiProgress, ProgressBar, ProgressDrawTarget};
use scraper::{ElementRef, Html};
use tokio::sync::mpsc::Sender;

use crate::{
    build_download_list::Download,
    download_throttle::{aquire_download_permit, rescale_permits},
    metadata::{is_opencast_video_stale, MetaData, MetaDataCache, MetaDataId},
    status::{bar_style, BarKind},
    VideoDownload,
};

pub async fn get_cmd_node(
    ilias: &Arc<Ilias>,
    download_client: &Arc<reqwest::Client>,
    video: &Download,
) -> String {
    let url = format!("{base_url}ilias.php?baseClass=ilObjPluginDispatchGUI&cmd=forward&ref_id={ref_id}&forwardCmd=showContent",
        base_url = ilias.base_url,
        ref_id = video.object.ref_id.0,
    );

    let res = download_client.get(url).send().await.unwrap();

    res.url()
        .query_pairs()
        .find(|(key, _)| key == "cmdNode")
        .expect("query did not contain 'cmdNode'")
        .1
        .into_owned()
}

fn take_valid_json_prefix(source: &str) -> &str {
    let mut input = source.trim();
    if let Some(remainder) = input.strip_prefix('{') {
        input = remainder.trim();
    } else {
        panic!("Not a JSON object");
    }

    let mut depth = 1;

    while depth > 0 {
        let (token, remainder) = next_json_token(input);
        input = remainder.trim();

        match token {
            "{" => depth += 1,
            "}" => depth -= 1,

            _ => {}
        }
    }

    &source[..(source.len() - input.len())]
}

fn next_json_token(input: &str) -> (&str, &str) {
    match input.chars().next().unwrap() {
        '{' | '}' | '[' | ']' | ',' | ':' => (&input[..1], &input[1..]),
        '"' => {
            let mut escaped = false;
            for (i, c) in input.char_indices().skip(1) {
                if escaped {
                    escaped = false;
                    continue;
                }

                if c == '\\' {
                    escaped = true;
                    continue;
                }

                if c == '"' {
                    return (&input[..=i], &input[i + 1..]);
                }
            }

            panic!("failed to parse string literal {input:?}");
        }
        d if d.is_numeric() => {
            let (i, _) = input
                .char_indices()
                .take_while(|(_, c)| c.is_numeric())
                .last()
                .unwrap();

            (&input[..=i], &input[i + 1..])
        }
        _ if input.starts_with("null") => (&input[..4], &input[4..]),
        _ => {
            panic!();
        }
    }
}

pub async fn download(
    ilias: &Arc<Ilias>,
    metadata: &Arc<Mutex<MetaDataCache>>,
    download_client: &Arc<reqwest::Client>,
    videos: Vec<Download>,
) -> anyhow::Result<Vec<VideoDownload>> {
    // let m3u_scraping_start = Instant::now();

    let pb = ProgressBar::new(videos.len() as u64).with_style(bar_style(BarKind::Numerical));
    pb.set_message("scraping video urls");

    let urls = join_all(videos.into_iter().map(|video| {
        let download_client = download_client.clone();
        let ilias = ilias.clone();
        let pb = pb.clone();
        tokio::spawn(async move {
            let cmd_node = get_cmd_node(&ilias, &download_client, &video).await;
            let url = build_video_url(&ilias, &cmd_node, video.object.ref_id);

            let _download_permit = aquire_download_permit().await;

            let res = download_client.get(url).send().await?.text().await?;
            pb.inc(1);
            anyhow::Result::<_>::Ok((video, res))
        })
    }))
    .await
    .into_iter()
    .map(|x| x.unwrap().unwrap())
    .flat_map(|(download, text)| {
        let download = Arc::new(download);
        let res = scrape_video_list(ilias, &Html::parse_fragment(&text));

        res.into_iter().map(move |video| (download.clone(), video))
    })
    .filter(|(download, video)| video.is_stale(&metadata.lock().unwrap(), download))
    .collect::<Vec<_>>();

    if urls.is_empty() {
        return Ok(Vec::new());
    }

    pb.reset();
    pb.set_position(0);
    pb.set_length(urls.len() as u64);
    pb.set_message("scraping m3u urls");

    let video_downloads: Vec<OpenCastDownload> =
        join_all(urls.into_iter().map(|(download, video)| {
            let pb = pb.clone();
            let download_client = download_client.clone();
            async move {
                let _download_permit = aquire_download_permit().await;

                let body: String = download_client
                    .get(&video.url)
                    .send()
                    .await
                    .unwrap()
                    .text()
                    .await
                    .unwrap();

                let json = take_valid_json_prefix(
                    body.split("xoctPaellaPlayer.init(").nth(1).unwrap().trim(),
                );

                let mut res: VideoData = serde_json::from_str(json).unwrap();

                let title = res.metadata.title.as_str();

                assert_eq!(
                    title, video.title,
                    "Video Display title was not the same as its metadata title"
                );

                assert!(!res.streams.is_empty(), "no streams found");

                res.streams.sort_by_key(|a| a.content == "presentation");

                let m3u_urls: Vec<String> = res
                    .streams
                    .iter()
                    .map(|stream| stream.sources.hls[0].src.clone())
                    .collect();

                let duration = res.metadata.duration;

                let title = title.trim().replace('/', "_");
                pb.inc(1);

                OpenCastDownload {
                    download,
                    video,
                    title,
                    m3u_urls,
                    duration,
                }
            }
        }))
        .await;

    pb.finish();

    // let total_len = video_downloads.iter().map(|x| x.duration).sum::<u64>();

    let multi_bar = Arc::new(MultiProgress::with_draw_target(
        // ProgressDrawTarget::hidden(),
        ProgressDrawTarget::stderr_nohz(),
    ));

    let mb = multi_bar.clone();
    let new_bar = Arc::new(move |kind: BarKind, len: u64| -> ProgressBar {
        mb.add(indicatif::ProgressBar::new(len).with_style(bar_style(kind)))
    });

    let mb = multi_bar.clone();
    let new_spinner =
        Arc::new(move || -> ProgressBar { mb.add(indicatif::ProgressBar::new_spinner()) });

    // let new_bar = |kind: BarKind, len: u64| -> ProgressBar { ProgressBar::hidden() };

    // HACK: force rendering of empty multi bar
    let _dummy_spinner = new_spinner();

    let mb = multi_bar.clone();
    tokio::task::spawn_blocking(move || mb.join());

    rescale_permits(8);

    let total_bar = new_bar(BarKind::Numerical, 0);
    total_bar.set_message("Total");

    let results = join_all(video_downloads.into_iter().map(|download| {
        async {
            let filename = download.title.trim().replace('/', "_");

            let _download_permit = aquire_download_permit().await;

            let folder = download
                .download
                .folder
                .join(download.download.object.title.trim().replace('/', "_"));

            tokio::fs::create_dir_all(&folder).await.unwrap();

            let path = folder.join(format!("{}.mkv", filename));

            let (status_sender, mut status_receiver) = tokio::sync::mpsc::channel(16);

            let downloads = join_all(download.m3u_urls.iter().enumerate().map(|(i, m3u_url)| {
                let folder = &folder;
                let filename = &filename;
                let status_sender = status_sender.clone();

                async move {
                    let part_temp_path = folder.join(format!(".~{}_{}.ts", filename, i));

                    download_hls(m3u_url, &part_temp_path, status_sender)
                        .await
                        .unwrap();
                    part_temp_path
                }
            }));

            let own_bar = new_bar(BarKind::Time, 0);
            own_bar.set_message(download.title.clone());

            let res = futures::future::select(
                downloads,
                Box::pin(async {
                    while let Some(status) = status_receiver.recv().await {
                        // eprintln!("{:?}", status);

                        match status {
                            HsldlStatus::Progress { delta, .. } => {
                                own_bar.inc(delta);
                                total_bar.inc(delta);
                            }
                            HsldlStatus::Length { delta, .. } => {
                                own_bar.inc_length(delta);
                                total_bar.inc_length(delta);
                            }
                        }

                        own_bar.tick();
                        total_bar.tick();
                    }
                }),
            )
            .await;

            let temp_files = {
                // move res into a local scope so that it is dropped after being partially moved
                let res = res;

                extract!(Either::Left((temp_files, _)) => temp_files, res).unwrap()
            };

            own_bar.finish();

            let ffmpeg_args = {
                let mut args = vec!["-y"];

                for temp_file in &temp_files {
                    args.extend_from_slice(&["-i", temp_file.to_str().unwrap()]);
                }

                args.extend_from_slice(&["-acodec", "copy", "-vcodec", "copy"]);

                let mut args: Vec<String> = args.into_iter().map(ToOwned::to_owned).collect();
                for (i, _) in temp_files.iter().enumerate() {
                    args.push("-map".to_owned());
                    args.push(format!("{}:a?", i));

                    args.push("-map".to_owned());
                    args.push(format!("{}:v", i));
                }

                args.push(path.to_str().unwrap().to_owned());

                args
            };

            own_bar.finish();

            multi_bar.remove(&own_bar);

            let own_spinner = new_spinner();
            own_spinner.set_message(format!("ffmpeg: {}", download.title));

            own_spinner.enable_steady_tick(1000 / 30);

            // println!("ffmpeg: {ffmpeg_args:?}");

            assert!(std::process::Command::new("ffmpeg")
                .args(ffmpeg_args)
                .stdout(std::process::Stdio::null())
                .stderr(std::process::Stdio::null())
                .spawn()
                .unwrap()
                .wait()
                .unwrap()
                .success());

            for temp_file in &temp_files {
                let _ = tokio::fs::remove_file(temp_file).await;
            }

            let mtime = download.video.start.timestamp();

            // dbg!(&path);

            filetime::set_file_mtime(&path, filetime::FileTime::from_unix_time(mtime, 0))
                .expect("Failed to set file mtime");

            // let file_size = out_path.metadata().unwrap().len();

            // eprintln!("Finished download of {}", download.title);

            metadata.lock().unwrap().insert(
                &MetaDataId::Video(
                    download.download.object.ref_id,
                    download.video.title.clone(),
                ),
                MetaData { mtime, path },
            );

            own_spinner.finish();

            multi_bar.remove(&own_spinner);

            download
        }
    }))
    .await;

    Ok(results
        .into_iter()
        .map(|download| VideoDownload {
            title: download.title,
            duration: Some(Duration::from_secs(download.duration / 1000)),
        })
        .collect::<Vec<_>>())
}

#[derive(Debug, Clone, Copy)]
enum HsldlStatus {
    Length { delta: u64, value: u64 },
    Progress { delta: u64, value: u64 },
}

async fn download_hls(
    m3u_url: &str,
    temp_file: impl AsRef<Path>,
    status: Sender<HsldlStatus>,
) -> std::io::Result<()> {
    use tokio::io::AsyncBufReadExt;

    // eprintln!("Starting download of {:?} from {}", temp_file.as_ref(), m3u_url);

    let mut child = tokio::process::Command::new("hlsdl")
        .args(&[
            "-b",
            "-v",
            "-f",
            "-o",
            temp_file.as_ref().to_str().unwrap(),
            m3u_url,
        ])
        .stderr(std::process::Stdio::piped())
        .spawn()
        .expect("Failed to run hlsdl");

    let child_out = tokio::io::BufReader::new(child.stderr.take().unwrap());

    let mut last = 0;
    let mut old_length = 0;
    let mut lines = child_out.lines();
    while let Some(line) = lines.next_line().await? {
        if line.starts_with('{') {
            let meta: serde_json::Value = serde_json::from_str(&line).unwrap();

            // eprintln!("\n");
            // dbg!(&meta);

            let t_d = meta.get("t_d").and_then(serde_json::Value::as_u64);
            let d_d = meta.get("d_d").and_then(serde_json::Value::as_u64);
            // let d_s = meta.get("d_s").map(serde_json::Value::as_u64).flatten();

            if let Some(t_d) = t_d {
                let new_length = t_d;

                if new_length != old_length {
                    let length_delta = new_length - old_length;

                    old_length = new_length;

                    status
                        .send(HsldlStatus::Length {
                            delta: length_delta,
                            value: new_length,
                        })
                        .await
                        .unwrap();
                }
            }

            if let Some(d_d) = d_d {
                let delta = d_d - last;

                last = d_d;

                status
                    .send(HsldlStatus::Progress { delta, value: d_d })
                    .await
                    .unwrap();
            }

            // eprintln!("{:?}", meta);
        } else {
            // Downloading part ...
            // eprintln!("{}", line);
        }
    }

    assert!(child.wait().await.unwrap().success());

    Ok(())
}

#[derive(Debug)]
struct OpenCastDownload {
    pub download: Arc<Download>,
    pub video: Video,
    pub title: String,
    pub m3u_urls: Vec<String>,
    pub duration: u64,
}

fn scrape_video_list(ilias: &Ilias, html: &Html) -> Vec<Video> {
    use chrono::TimeZone;
    const DATE_TIME_FORMAT: &str = "%d.%m.%Y - %H:%M";

    mod selector {
        use once_cell::sync::Lazy as L;
        use scraper::Selector as S;

        pub static HEADER_FIELD: L<S> = L::new(|| S::parse(":scope thead tr th").unwrap());
        pub static ROW: L<S> = L::new(|| S::parse(":scope tbody tr").unwrap());
        pub static COLUMN: L<S> = L::new(|| S::parse(":scope td").unwrap());
        pub static ANCHOR: L<S> = L::new(|| S::parse("a").unwrap());
        pub static TABLE: L<S> = L::new(|| S::parse("div.ilTableOuter table").unwrap());
    }

    let table = {
        let mut tables = html.select(&selector::TABLE);
        let table = tables.next().expect("No content table found in index");
        assert!(tables.next().is_none(), "More than one content table found");
        table
    };

    let headers: Vec<String> = table
        .select(&selector::HEADER_FIELD)
        .map(|th| th.text().collect::<String>().trim().to_owned())
        .collect();

    let mut fields = vec![Vec::new(); headers.len()];

    for (i, row) in table.select(&selector::ROW).enumerate() {
        let mut columns = row.select(&selector::COLUMN);

        for (field, value) in fields.iter_mut().zip(&mut columns) {
            field.push(value);
        }

        if columns.next().is_some() {
            // something is wrong

            // check if the table is empty
            if i == 0 {
                let mut columns = row.select(&selector::COLUMN);

                let first = columns.next().expect("empty table row");

                if columns.next().is_none() {
                    let text: String = first.text().collect();

                    if text.contains("empty") || text.contains("Keine") {
                        break;
                    }
                }
            }

            panic!("More colums than table headers");
        }
    }

    let get_field = |target_title: &str| -> &[ElementRef] {
        &fields[headers
            .iter()
            .enumerate()
            .find_map(|(i, title)| (title == target_title).then(|| i))
            .unwrap_or_else(|| panic!("Table had no '{}' column", target_title))]
    };

    let urls = get_field("Events").iter().map(|events| {
        events.select(&selector::ANCHOR).find_map(|a| {
            (a.text().map(str::trim).collect::<String>() == "Play").then(|| {
                format!(
                    "{base_url}{path}",
                    base_url = ilias.base_url,
                    path = a
                        .value()
                        .attr("href")
                        .expect("`Play` anchor had no `href`")
                        .trim()
                )
            })
        })
    });

    let start_times = get_field("Start")
        .iter()
        .map(|title| title.text().map(str::trim).collect::<String>())
        .map(|datetime| {
            Local
                .datetime_from_str(&datetime, DATE_TIME_FORMAT)
                .expect("Unexpected date/time format")
        });

    let titles = get_field("Title").iter().map(|title| {
        title
            .children()
            .filter_map(|x| x.value().as_text().map(|x| x.to_string().trim().to_owned()))
            .collect::<String>()
    });

    urls.into_iter()
        .zip(start_times)
        .zip(titles)
        .filter_map(|((url, start), title)| url.map(|url| Video { url, start, title }))
        .collect()
}

#[derive(Debug)]
pub struct Video {
    pub url: String,
    pub start: DateTime<Local>,
    pub title: String,
}

impl Video {
    fn is_stale(&self, metadata: &MetaDataCache, download: &Download) -> bool {
        is_opencast_video_stale(download, self, metadata)
    }
}

fn build_video_url(ilias: &Ilias, cmd_node: &str, ref_id: RefId) -> String {
    format!("{base_url}ilias.php?ref_id={ref_id}&baseClass=ilObjPluginDispatchGUI&cmd=asyncGetTableGUI&cmdClass=xocteventgui&cmdNode={cmd_node}&cmdMode=asynch&lang=en&limit=9999", 
        base_url = ilias.base_url,
        ref_id = ref_id.0,
        cmd_node = cmd_node,
    )
}

#[derive(Debug, serde::Deserialize)]
struct VideoData {
    #[serde(skip)]
    #[serde(alias = "frameList")]
    frame_list: Vec<Frame>,
    metadata: Metadata,
    streams: Vec<Stream>,
}

#[derive(Debug, serde::Deserialize)]
struct Frame {
    id: String,
    mimetype: String,
    thumb: String,
    time: u64,
    url: String,
}

#[derive(Debug, serde::Deserialize)]
struct Metadata {
    duration: u64,
    preview: String,
    title: String,
}

#[derive(Debug, serde::Deserialize)]
struct Sources {
    #[serde(skip)]
    dash: Vec<Source>,
    hls: Vec<Source>,
}

#[derive(Debug, serde::Deserialize)]
struct Source {
    mimetype: String,
    src: String,
}

#[derive(Debug, serde::Deserialize)]
struct Stream {
    content: String,
    sources: Sources,
}
