#![allow(dead_code)]
#![warn(clippy::pedantic)]
#![allow(clippy::let_underscore_drop, clippy::too_many_lines)]

use ilias_soap::{GetCoursesStatus, Ilias, ObjectType};
use reqwest::Url;
use std::{
    path::PathBuf,
    sync::{Arc, Mutex},
    time::{Duration, Instant},
};
use tokio::io::AsyncReadExt;

use crate::{build_download_list::gather_downloads, metadata::MetaDataCache};

#[macro_export]
macro_rules! extract {
    ($src: pat $(if $guard: expr)? => $dest: expr, $val: expr $(,)?) => {
        match $val {
            $src $(if $guard)? => ::core::option::Option::Some($dest),
            _ => ::core::option::Option::None,
        }
    };
}

#[derive(Debug)]
pub struct VideoDownload {
    pub title: String,
    pub duration: Option<Duration>,
}

#[derive(serde::Deserialize)]
struct Config {
    username: String,
    password: String,
    out_path: String,
    newer_than: Option<String>,
    target_semester: Option<String>,
    download_files: bool,
    download_videos: bool,
}

impl Config {
    async fn load(config_path: &str) -> Self {
        let mut config = String::new();

        tokio::fs::File::open(&config_path)
            .await
            .unwrap()
            .read_to_string(&mut config)
            .await
            .unwrap();

        serde_json::de::from_str(&config).unwrap()
    }
}

fn setup_cookie_jar(ilias: &Ilias) -> reqwest::cookie::Jar {
    let cookie_store = reqwest::cookie::Jar::default();
    cookie_store.add_cookie_str(
        &format!("PHPSESSID={}", ilias.sessid()),
        &Url::parse(ilias.base_url).unwrap(),
    );
    cookie_store.add_cookie_str(
        &format!("ilClientId={}", ilias.client()),
        &Url::parse(ilias.base_url).unwrap(),
    );
    cookie_store
}

mod build_download_list;
mod download_throttle;
mod files;
mod mediacast_videos;
mod metadata;
mod opencast_videos;
mod status;

#[tokio::main]
async fn main() -> anyhow::Result<()> {
    let home = std::env::vars()
        .find_map(|(key, value)| (key == "HOME").then(|| value))
        .expect("HOME is not set");

    let config_path = format!("{}/.config/ilias_downloader/config.json", home);
    let metadata_path = format!("{}/.config/ilias_downloader/mtime_cache.json.gz", home);

    let metadata = MetaDataCache::load(&metadata_path).unwrap();

    let Config {
        username,
        password,
        out_path,
        newer_than,
        target_semester,
        download_files,
        download_videos,
    } = Config::load(&config_path).await;

    let newer_than = newer_than.map(|x| x.parse().expect("Failed to parse \"newer than\" config"));

    let ilias = Arc::new(Ilias::login(&username, &password).await?);

    let courses = ilias.get_courses(GetCoursesStatus::MEMBER).await?;

    let base_path = PathBuf::from(out_path);

    let metadata = Arc::new(Mutex::new(metadata));
    let download_client = Arc::new(
        reqwest::ClientBuilder::new()
            .cookie_provider(Arc::new(setup_cookie_jar(&ilias)))
            .build()
            .unwrap(),
    );

    let tree_build_start = Instant::now();

    let downloads =
        gather_downloads(&ilias, &base_path, courses, newer_than, target_semester).await;

    eprintln!(
        "building directory tree took {:#.2?}",
        tree_build_start.elapsed()
    );

    let (files, opencast_videos, mediacast_videos) = {
        let mut files = Vec::new();
        let mut opencast_videos = Vec::new();
        let mut mediacast_videos = Vec::new();
        let mut excercises = Vec::new();
        let mut web_references = Vec::new();

        for download in downloads {
            match download.object.r#type {
                ObjectType::File => files.push(download),
                ObjectType::OpenCast => opencast_videos.push(download),
                ObjectType::MediaCast => mediacast_videos.push(download),
                ObjectType::Excercise => excercises.push(download),
                ObjectType::WebReference => web_references.push(download),

                _ => panic!(
                    "download list contained unexpected element: {:#?}",
                    download
                ),
            }
        }

        // dbg!(&excercises);

        // for excercise in &excercises {
        //     dbg!(ilias.get_exercise(excercise.object.ref_id, ilias_soap::AttachmentMode::Plain).await);
        // }

        (files, opencast_videos, mediacast_videos)
    };

    // dbg!(mediacast_videos);

    let files = download_files.then(|| files).unwrap_or_default();

    let new_files = files::download(&ilias, &metadata, &download_client, files).await?;

    let opencast_videos = download_videos.then(|| opencast_videos).unwrap_or_default();
    let mediacast_videos = download_videos
        .then(|| mediacast_videos)
        .unwrap_or_default();

    let new_opencast_videos =
        opencast_videos::download(&ilias, &metadata, &download_client, opencast_videos).await?;

    let new_mediacast_videos =
        mediacast_videos::download(&ilias, &metadata, &download_client, mediacast_videos).await?;

    let new_videos = {
        let mut new_mediacast_videos = new_mediacast_videos;
        let new_opencast_videos = new_opencast_videos;

        new_mediacast_videos.extend(new_opencast_videos.into_iter());
        new_mediacast_videos
    };

    let (n_files, n_videos) = (new_files.len(), new_videos.len());

    if !new_files.is_empty() {
        eprintln!("=== \x1b[34mnew files\x1b[m ===");

        for (file_size, file) in new_files {
            eprintln!(
                "- [{:>10}]: {}",
                format!("{}", indicatif::HumanBytes(file_size)),
                file.object.title
            );
        }
    }

    if !new_videos.is_empty() {
        eprintln!("=== \x1b[34mnew videos\x1b[m ===");

        for video in new_videos {
            if let Some(duration) = video.duration {
                eprintln!(
                    "- [{:>11}]: {:?}",
                    indicatif::HumanDuration(duration),
                    video.title
                );
            } else {
                eprintln!("- [  unknown  ]: {:?}", video.title);
            }
        }
    }

    eprintln!();

    eprintln!(
        "summary: downloaded \x1b[34m{}\x1b[m file{} and \x1b[34m{}\x1b[m video{}.",
        n_files,
        if n_files == 1 { "" } else { "s" },
        n_videos,
        if n_videos == 1 { "" } else { "s" }
    );

    Ok(())
}
