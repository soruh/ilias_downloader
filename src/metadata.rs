use std::{
    collections::HashMap,
    io::BufReader,
    path::{Path, PathBuf},
};

use flate2::{bufread::GzDecoder, write::GzEncoder, Compression};
use ilias_soap::RefId;

use crate::{build_download_list::Download, opencast_videos::Video};

#[derive(Debug, serde_derive::Serialize, serde_derive::Deserialize)]
pub struct MetaData {
    pub mtime: i64,
    pub path: PathBuf,
}

pub fn is_file_stale(download: &Download, metadata: &MetaDataCache) -> bool {
    let metadata = metadata.get(&MetaDataId::File(download.object.ref_id));

    is_download_stale(
        &download.folder,
        download.object.last_update.timestamp(),
        metadata,
    )
}

pub fn is_opencast_video_stale(
    download: &Download,
    video: &Video,
    metadata: &MetaDataCache,
) -> bool {
    let metadata = metadata.get(&MetaDataId::Video(
        download.object.ref_id,
        video.title.clone(),
    ));

    let folder = download
        .folder
        .join(download.object.title.trim().replace('/', "_"));

    is_download_stale(&folder, video.start.timestamp(), metadata)
}

pub fn is_mediacast_video_stale(
    download: &Download,
    metadata: &MetaDataCache,
    title: String,
) -> bool {
    let metadata = metadata.get(&MetaDataId::Video(download.object.ref_id, title));

    is_download_stale(
        &download.folder,
        download.object.last_update.timestamp(),
        metadata,
    )
}

#[derive(Debug, Default, serde_derive::Serialize, serde_derive::Deserialize)]
pub struct MetaDataCache {
    path: PathBuf,
    content: HashMap<String, MetaData>,
}

#[derive(Debug, Hash, Clone, PartialEq, Eq, serde_derive::Serialize, serde_derive::Deserialize)]
pub enum MetaDataId {
    File(RefId),
    Video(RefId, String),
}

impl MetaDataCache {
    pub fn load(metadata_path: impl AsRef<Path>) -> anyhow::Result<Self> {
        let metadata_path = metadata_path.as_ref().to_owned();

        let file = std::fs::File::open(&metadata_path);

        Ok(Self {
            path: metadata_path,
            content: if let Ok(file) = file {
                serde_json::de::from_reader(GzDecoder::new(BufReader::new(file)))?
            } else {
                HashMap::new()
            },
        })
    }

    pub fn save(&mut self) -> anyhow::Result<()> {
        let temp_dir = self.path.parent().unwrap();
        std::fs::create_dir_all(temp_dir)?;

        let temp_path = PathBuf::from(format!("{}.temp", self.path.to_str().unwrap()));
        {
            let mut writer =
                GzEncoder::new(std::fs::File::create(&temp_path)?, Compression::best());

            serde_json::ser::to_writer(&mut writer, &self.content)?;
        }

        std::fs::copy(&temp_path, &self.path)?;
        std::fs::remove_file(&temp_path).unwrap();

        Ok(())
    }

    pub fn insert(&mut self, k: &MetaDataId, v: MetaData) {
        self.content.insert(format!("{:?}", k), v);

        self.save().unwrap();
    }

    pub fn get(&self, k: &MetaDataId) -> Option<&MetaData> {
        self.content.get(&format!("{:?}", k))
    }
}

pub fn is_download_stale(path: &Path, timestamp: i64, metadata: Option<&MetaData>) -> bool {
    if let Some(metadata) = metadata {
        if timestamp > metadata.mtime {
            return true;
        }

        if metadata.path.parent() != Some(path) {
            return true;
        }

        if let Ok(meta) = metadata.path.metadata() {
            let file_timestamp = meta
                .modified()
                .unwrap()
                .duration_since(std::time::UNIX_EPOCH)
                .unwrap()
                .as_secs()
                .try_into()
                .unwrap();

            timestamp > file_timestamp
        } else {
            true
        }
    } else {
        true
    }
}
